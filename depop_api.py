# standard imports
import logging
import functools

# 3rd party imports
import requests

logging.basicConfig(
  level=logging.INFO,
  format='%(asctime)s.%(msecs)03d %(message)s',
  datefmt='%Y-%m-%d %H:%M:%S',
)
LOGGER = logging.getLogger()

API_DOMAIN = 'webapi.depop.com'
API_PREFIX = f'https://{API_DOMAIN}/api'

class BadAuthorizationException(Exception):
  pass

class DepopAPI:
  def __init__(self, auth_token):
    self.auth_token = auth_token
    self.auth_header = {'authorization': f'Bearer {self.auth_token}'}

  @staticmethod
  def get_user_id(username):
    """
    Converts the specified username into an integer ID

    :username: str: The specified username
    :returns: int:  Integer ID of specified user
    :returns: None: If lookup of the specified user fails
    """
    response = requests.get(
      f'{API_PREFIX}/v1/shop/{username}'
    )
    if response.status_code == 200:
      user_id = response.json()['id']
      LOGGER.info(f"successfully found user '{username}' (ID={user_id})")
      return user_id
    else:
      LOGGER.info(f"failed to find user '{username}'")
      return None

  @staticmethod
  def check_username(username):
    return DepopAPI.get_user_id(username) is not None

  def get_item_data(self, item_slug):
    """
    Fetch dictionary containing information about the specified item.

    :item_slug: str:    The specified item
    :returns:   dict:   Dictionary containing information about item slug
    :returns:   None:   If item slug is invalid
    :raises:    BadAuthorizationException:  If the provided `auth_token` is invalid
    """
    response = requests.get(
      f'{API_PREFIX}/v2/products/{item_slug}',
      headers=self.auth_header
    )
    if response.status_code == 200:
      return response.json()
    elif response.status_code == 401:
      raise BadAuthorizationException
    else:
      LOGGER.info(f"failed to find item '{item_slug}' [code={response.status_code}]")
      return None

  def get_item_page(self, index, user_id, limit=200, offset_id=None):
    """
    Fetches a page from the specified user. Use the offset_id to fetch
    sequential pages.

    :index:     int:        Index of page relative to other pages; only used for logging purposes
    :user_id:   int:        ID of the specified user
    :limit:     int:        Maximum number of items to return. [default=24]
    :offset_id: str|None:   Returned by calls to this function in the 'meta->last_offset_id' field.
                            Used to get sequential pages. See `get_unsold_items` for example
    :returns:   dict:       Page of item data
    :returns:   None:       If the page request fails
    :raises:    BadAuthorizationException: If the provided `auth_token` is invalid
    """
    params = {'limit': limit}
    if offset_id:
      params |= {'offset_id': offset_id}

    response = requests.get(
      f'{API_PREFIX}/v1/shop/{user_id}/products/',
      headers=self.auth_header,
      params=params
    )

    if response.status_code == 200:
      item_page = response.json()
      n_loaded = len(item_page['products'])
      LOGGER.info(f"successfully loaded {n_loaded} items from page {index} of user ID={user_id}")
      return item_page
    elif response.status_code == 401:
      raise BadAuthorizationException
    else:
      LOGGER.info(f"failed to load product page of user {user_id}")
      return None

  def get_unsold_items(self, user_id):
    """
    Fetches the item-slugs of all unsold items for the specified user

    :user_id: int:      ID of the specified user
    :returns: [str]:    List of item-slugs
    :raises: BadAuthorizationException: If the provided `auth_token` is invalid
    """
    unsold_items = []
    n_total_items = 0
    n_sold_items = 0

    page_index = 1
    LOGGER.info(f"attempting to gather list of all unsold items of user ID={user_id}")
    item_page = self.get_item_page(page_index, user_id)
    if not item_page:
      return None

    while True:
      n_total_items += len(item_page['products'])
      for product in item_page['products']:
        if product['sold']:
          n_sold_items += 1
        else:
          unsold_items.append(product['slug'])

      offset_id = item_page['meta']['last_offset_id']
      if item_page['meta']['end']:
        break

      page_index += 1
      item_page = self.get_item_page(page_index, user_id, offset_id=offset_id)

    LOGGER.info(f"finished gathering list of all unsold items of user ID={user_id}")
    LOGGER.info(f'total items: {n_total_items}')
    LOGGER.info(f'total unsold items: {len(unsold_items)}')
    LOGGER.info(f'total sold items: {n_sold_items}')
    return unsold_items

  def refresh_item(self, item_slug):
    """
    Refresh the specified item. Assumes user has authority to do so.

    :item_slug: str:            The specified item
    :raises:    ValueError:     If item slug is invalid
    :raises:    BadAuthorizationException:  If the provided `auth_token` is invalid
    :raises:    RuntimeError:               If request fails for any other reason
    """
    item_data = self.get_item_data(item_slug)
    if not item_data:
      raise ValueError(f"couldn't look up item '{item_slug}'")

    for size in item_data['variants'].keys():
      item_data['variants'][size] = 0

    response = requests.put(
      f'{API_PREFIX}/v1/products/{item_slug}',
      headers=self.auth_header,
      json={
        'variants': item_data['variants'],
        "address": item_data['address'],
        'countryCode': item_data['countryCode'],
      }
    )
    if response.status_code == 201:
      LOGGER.info(f"successfully refreshed item '{item_slug}'")
    elif response.status_code == 401:
      raise BadAuthorizationException
    else:
      raise RuntimeError(f"depop returned error code {response.status_code}")

  def delete_item(self, item_slug):
    item_data = self.get_item_data(item_slug)
    if not item_data:
      raise ValueError(f"couldn't look up item '{item_slug}'")

    item_id = item_data['id']

    response = requests.delete(
      f'{API_PREFIX}/v1/products/{item_id}',
      headers=self.auth_header
    )
    if response.status_code == 204:
      LOGGER.info(f"successfully deleted item '{item_slug}'")
    else:
      raise RuntimeError(f"depop returned error code {response.status_code}")

  def get_item_image_urls(self, item_data):
    """
    get list URLs for an item's images
    """
    urls = []
    for picture in item_data['pictures']:
      max_res = 0
      max_res_url = None
      for copy in picture:
        # images are squares, so only need to check one dimension
        if copy['width'] > max_res:
          max_res = copy['width']
          max_res_url = copy['url']
      urls.append(max_res_url)
    return urls

  def upload_image(self, image_bytes):
    """
    upload an image to depop's servers. returns ID number of image after uploading.
    """
    location_response = requests.post(
      f'{API_PREFIX}/v2/pictures/',
      headers=self.auth_header,
      json={
        'type': 'product',
        'extension': 'jpg'
      }
    )
    if location_response.status_code != 201:
      raise RuntimeError(f"depop returned error code {location_response.status_code}")

    location_response_data = location_response.json()

    upload_response = requests.put(
      location_response_data['url'],
      headers={'Content-Type': 'image/jpeg'},
      data=image_bytes
    )
    if upload_response.status_code != 200:
      raise RuntimeError(f"depop returned error code {upload_response.status_code}")

    return location_response_data['id']

  def relist_item(self, item_slug, delete_old=False):
    old_item_data = self.get_item_data(item_slug)
    if not old_item_data:
      raise ValueError(f"couldn't look up item '{item_slug}'")

    post_to_get = {
      'description': None,
      'group': None,
      'productType': None,
      'attributes': None,
      'gender': None,
      'variantSetId': None,
      'nationalShippingCost': ['price', 'nationalShippingCost'],
      'internationalShippingCost': ['price', 'internationalShippingCost'],
      'priceAmount': ['price', 'priceAmount'],
      'variants': None,
      'brand': None,
      'brandId': None,
      'categoryId': None,
      'condition': None,
      'colour': None,
      'source': None,
      'age': None,
      'style': None,
      'shippingMethods': None,
      'priceCurrency': ['price', 'currencyName'],
      'address': None,
      'countryCode': None,
      'isKids': None,
    }

    new_image_ids = []
    for image_url in self.get_item_image_urls(old_item_data):
      image_data = requests.get(image_url).content
      image_id = self.upload_image(image_data)
      new_image_ids.append(image_id)

    new_item_data = {
      'pictureIds': new_image_ids,
      'geoLat': -50.000, # TODO: figure out if this really matters
      'geoLng': 122.000, # TODO: figure out if this really matters
    }

    for post_field, get_field in post_to_get.items():
      if get_field is None:
        if post_field in old_item_data:
          new_item_data[post_field] = old_item_data[post_field]
      else:
        new_item_data[post_field] = functools.reduce(dict.get, get_field, old_item_data)

    response = requests.post(
      f'{API_PREFIX}/v2/products/',
      headers=self.auth_header,
      json=new_item_data
    )

    if response.status_code == 201:
      new_slug = response.json()['slug']
      LOGGER.info(f"successfully relisted item '{item_slug}' as '{new_slug}'")
      if delete_old:
        self.delete_item(item_slug)
      return new_slug
    else:
      raise RuntimeError(f"depop returned error code {response.status_code}")
